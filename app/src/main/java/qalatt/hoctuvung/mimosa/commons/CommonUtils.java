package qalatt.hoctuvung.mimosa.commons;



public class CommonUtils {

    private static final long ON_CLICK_EVENT_DURATION_TIME = 1000;
    private static long sClickTime = 0;

    public static boolean singleClickEvent() {
        long time = System.currentTimeMillis();
        if (time - sClickTime < ON_CLICK_EVENT_DURATION_TIME) {
            return false;
        }
        sClickTime = time;
        return true;
    }

}
