package qalatt.hoctuvung.mimosa.commons;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import qalatt.hoctuvung.mimosa.managers.EConvarSApplication;


public class ConnectivityUtil {

        public static boolean isNetworkConnected() {
            Context context = EConvarSApplication.getInstance().getApplicationContext();
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = cm.getActiveNetworkInfo();

            return ni != null && ni.isConnected();
        }

        public static String getConnectedType() {
            Context context = EConvarSApplication.getInstance().getApplicationContext();
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = cm.getActiveNetworkInfo();
            if (ni != null) {
                String type = ni.getTypeName();
                String type2 = ni.getSubtypeName();
                if (ni.isConnected()) {
                    if (type != null && type.equals("WIFI")) {
                        return type;
                    }
                    if (type2 != null && type != null && type.equals("mobile")) {
                        return type2;
                    }
                }
            }

            return null;
        }
}
