package qalatt.hoctuvung.mimosa.commons;

public class ShareKey {
    public final static String TOKEN = "TOKEN";
    public final static String DEVICE_TOKEN = "DEVICE_TOKEN";
    public final static int STATUS_200_OK = 200;
    public final static int STATUS_201_OK = 201;
    public final static int STATUS_400 = 400;
    public final static String ID_USER = "ID_USER";
    public final static String ID_CATEGORY = "ID_CATEGORY";
    public final static String DATAVOCABUNARY = "DATAVOCABUNARY";
    public final static String STAGETHREE = "STAGETHREE";
    public final static String NAMECATE = "NAMECATE";
    public final static String IMAGECATE = "IMAGECATE";
    public final static String STAGE1 = "STAGE1";
    public final static String STAGE2 = "STAGE2";
    public final static String STAGE3 = "STAGE3";

}
