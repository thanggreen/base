package qalatt.hoctuvung.mimosa.networkings;


import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import qalatt.hoctuvung.mimosa.BuildConfig;
import qalatt.hoctuvung.mimosa.commons.AppConstants;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {
    private static ApiInterface sAppService;
//    private String domain = DataManager.getInstance().getStringSharedOreferences("DOMAIN").trim().replaceAll("\"", "");

    public ApiInterface getApiClient(final String string) {
        if (sAppService == null) {
            //okhttp client
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    //header設定
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Accept", "application/json")
                            .method(original.method(), original.body());
                    if (BuildConfig.DEBUG) {
                        requestBuilder.addHeader("Authorization", string);
                    }

                    return chain.proceed(requestBuilder.build());
                }
            });

            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                httpClient.addInterceptor(logging);
            }

            LogJsonInterceptor jsonInterceptor = new LogJsonInterceptor();
            httpClient.addInterceptor(jsonInterceptor);

            //Gson
            Gson gson = new GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .registerTypeAdapter(Date.class, new DateTypeAdapter())
                    .create();

            httpClient
                    .readTimeout(AppConstants.REQUEST_TIMEOUT_SECOND, TimeUnit.SECONDS)
                    .connectTimeout(AppConstants.REQUEST_TIMEOUT_SECOND, TimeUnit.SECONDS);

            OkHttpClient client = httpClient.build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.Domain)
                    .addConverterFactory(GsonConverterFactory.create(gson))//Gsonの使用
                    .client(client)
                    .build();

            sAppService = retrofit.create(ApiInterface.class);
        }

        return sAppService;
    }

    public interface ApiInterface {

//        @GET(AppConstants.API_LOGIN)
//        Call<LoginResponse> loginEnglish(@Header("access-token") String token);

    }
}
