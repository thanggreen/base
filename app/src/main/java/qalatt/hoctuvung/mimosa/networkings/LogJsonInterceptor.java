package qalatt.hoctuvung.mimosa.networkings;

import android.os.Handler;
import android.os.Looper;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import qalatt.hoctuvung.mimosa.commons.LogUtils;



public class LogJsonInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        final Request request = chain.request();
        Response response;
        try {
            response = chain.proceed(request);
            final String rawJson = response.body().string();
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    String label = request.url().toString() + ((request.body() != null) ? "?" + LogJsonInterceptor.bodyToString(request.body()) : "");
                    LogUtils.d(LogJsonInterceptor.class.getSimpleName(),label);
                }
            });
            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), rawJson)).build();
        } catch (IOException e) {
            LogUtils.d(LogJsonInterceptor.class.getSimpleName(),e.getMessage());
        }
        return chain.proceed(request);
    }

    public static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            copy.writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            LogUtils.d(LogJsonInterceptor.class.getSimpleName(),e.getMessage());
            return "";
        }
    }
}