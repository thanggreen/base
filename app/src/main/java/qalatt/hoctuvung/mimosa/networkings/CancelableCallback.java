package qalatt.hoctuvung.mimosa.networkings;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import qalatt.hoctuvung.mimosa.commons.LogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public abstract class CancelableCallback<T> implements Callback<T> {

    private static List<CancelableCallback> mList = new ArrayList<>();

    private boolean isCanceled = false;
    private Object mTag = null;

    public CancelableCallback() {
        mList.add(this);
    }
    public ApiClient mApiClient = new ApiClient();
    public CancelableCallback(Object tag) {
        mTag = tag;
        mList.add(this);
    }

    public static void cancelAll() {
        for (CancelableCallback callback : mList) {
            callback.cancel();
        }
    }

    public static void cancel(Object tag) {
        if (tag != null)
            for (CancelableCallback callback : mList) {
                if (tag.equals(callback.mTag))
                    callback.cancel();
            }
    }

    public void cancel() {
        isCanceled = true;
//        mList.remove(this);
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        // response.isSuccessful() is true if the response code is 2xx
        if (!isCanceled) {
            if (response.isSuccessful()) {
                onSuccess(response.body(), response);
            } else {
                RetrofitError error = new RetrofitError(response.raw().request().url().url().toString(), response);
                LogUtils.d(CancelableCallback.class.getSimpleName(), String.format("url=%s, code=%d, message=%s", error.getUrl(), error.getResponse().code(), error.getResponse().message()));
                onFailure(error);
            }
        }
        mList.remove(this);
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        t.printStackTrace();
        RetrofitError error = new RetrofitError(t);
        Throwable throwable = error.getCause();
        if(throwable instanceof UnknownHostException || throwable instanceof ConnectException){
            //offline.
            String label = call.request().url().toString() + ((call.request().body() !=null)? "?" +LogJsonInterceptor.bodyToString(call.request().body()): "");
            LogUtils.d(CancelableCallback.class.getSimpleName(), label);
        }else if(throwable instanceof TimeoutException || throwable instanceof SocketTimeoutException){
            //TimeOut
            String label = call.request().url().toString() + ((call.request().body() !=null)? "?" +LogJsonInterceptor.bodyToString(call.request().body()): "");
            LogUtils.d(CancelableCallback.class.getSimpleName(), label);
        }
        if (!isCanceled)
            onFailure(error);
        LogUtils.d(CancelableCallback.class.getSimpleName(), String.format("url=%s, code=%d, message=%s", error.getUrl(), -1, error.getMessage()));
        mList.remove(this);
    }

    public abstract void onSuccess(T t, Response response);

    public abstract void onFailure(RetrofitError error);
}