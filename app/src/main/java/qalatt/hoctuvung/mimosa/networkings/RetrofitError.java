package qalatt.hoctuvung.mimosa.networkings;

import retrofit2.Response;



public class RetrofitError extends Exception {
    private String mUrl = null;
    private Response mResponse = null;

    public RetrofitError(Throwable cause) {
        super(cause);
        mUrl = null;
        mResponse = null;
    }

    public RetrofitError(String url, Response response) {
        super();
        mUrl = url;
        mResponse = response;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public Response getResponse() {
        return mResponse;
    }

    public void setResponse(Response response) {
        mResponse = response;
    }
}
