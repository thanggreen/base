package qalatt.hoctuvung.mimosa.interfaces;

import java.util.EventListener;

/**
 * Created by manh on 6/15/18.
 */

public interface DialogListener extends EventListener {

    void doPositiveClick();

    void doNegativeClick();

}