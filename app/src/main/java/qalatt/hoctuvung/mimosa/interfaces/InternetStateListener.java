package qalatt.hoctuvung.mimosa.interfaces;


public interface InternetStateListener {
    void isNetworkAvailable(boolean isNetworkAvailable);
}
