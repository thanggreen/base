package qalatt.hoctuvung.mimosa.views.activites;


import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.List;

import qalatt.hoctuvung.mimosa.R;
import qalatt.hoctuvung.mimosa.interfaces.DialogListener;
import qalatt.hoctuvung.mimosa.managers.DataManager;
import qalatt.hoctuvung.mimosa.views.customviews.CommonDialogFragment;
import qalatt.hoctuvung.mimosa.views.fragments.BaseFragment;

public class BaseActivity extends AppCompatActivity implements BaseFragment.ChangeFragmentListener, BaseFragment.ChangeToolBarListener, DialogListener {
    public static final int ANIMATION_NONE = 0;
    public static final int ANIMATION_SLIDE_RIGHT_TO_LEFT = 1;
    public static final int ANIMATION_SLIDE_TOP_TO_BOTTOM = 2;
    public static final int ANIMATION_SLIDE_BOTTOM_TO_TOP = 3;
    public static final int ANIMATION_SLIDE_LEFT_TO_RIGHT = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("");
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count != 1) {
            getSupportFragmentManager().popBackStack();
            BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.mainContainer);
            fragment.onBackPressed();
            return;
        } else {
            System.exit(0);
        }

        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void setFragment(BaseFragment fragment, int animation) {
        FragmentTransaction transaction = getCustomFragmentTransaction(animation);
        transaction.add(R.id.mainContainer, fragment, fragment.FRAGMENT_TAG);
        transaction.commit();
    }

    @Override
    public void addFragmentWithTarget(BaseFragment parent, BaseFragment child, int animation, int fragmentCode) {
        child.setTargetFragment(parent, fragmentCode);
        FragmentTransaction transaction = getCustomFragmentTransaction(animation);
        transaction.addToBackStack(child.FRAGMENT_TAG);
        transaction.add(R.id.mainContainer, child, child.FRAGMENT_TAG);
        transaction.commit();
    }


    public void setFragment(BaseFragment fragment, int animation, int resId) {
        FragmentTransaction transaction = getCustomFragmentTransaction(animation);
        transaction.add(resId, fragment, fragment.FRAGMENT_TAG);
        transaction.commit();
    }


    @Override
    public void addFragment(BaseFragment fragment, int animation) {
        FragmentTransaction transaction = getCustomFragmentTransaction(animation);
        transaction.add(R.id.mainContainer, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }


    public void addFragment(BaseFragment fragment, int animation, int resId) {
        FragmentTransaction transaction = getCustomFragmentTransaction(animation);
        transaction.add(resId, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public void changeFragment(BaseFragment fragment, int animation) {
        FragmentTransaction transaction = getCustomFragmentTransaction(animation);
        transaction.replace(R.id.mainContainer, fragment, fragment.FRAGMENT_TAG);
        transaction.commit();
    }


    public void changeFragment(BaseFragment fragment, int animation, int resId) {
        FragmentTransaction transaction = getCustomFragmentTransaction(animation);
        transaction.replace(resId, fragment, fragment.FRAGMENT_TAG);
        transaction.commit();
    }


    @Override
    public void replaceChildFragment(BaseFragment fragment, int animation, BaseFragment parentFragment) {
        FragmentTransaction transaction = getCustomFragmentTransaction(animation);
        transaction.remove(parentFragment);
        transaction.add(R.id.mainContainer, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }


    public void replaceChildFragment(BaseFragment fragment, int animation, BaseFragment parentFragment, int resId) {
        FragmentTransaction transaction = getCustomFragmentTransaction(animation);
        transaction.remove(parentFragment);
        transaction.add(resId, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }


    @Override
    public void popBackStackInclusiveFragment(String stackName) {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(stackName, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }


    @Override
    public void popBackStackFragment(String stackName) {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(stackName, 0);
    }


    @Override
    public void popBackStackFragments() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count != 1) {
            getSupportFragmentManager().popBackStack();
            BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.mainContainer);
            fragment.onBackPressed();
            return;
        }
    }


    @Override
    public void popToRootFragments() {
        FragmentManager fm = getSupportFragmentManager();
        while (fm.getBackStackEntryCount() > 1) {
            fm.popBackStackImmediate();
        }
    }

    @Override
    public void showDialogFragment(int type, String tag) {
        String title = "";
        String message = "";
        int dialogType;
        switch (type) {
            case BaseFragment.DIALOG_TYPE_NETWORK_ERROR:
                title = getResources().getString(R.string.network_err_dialog_title);
                message = getResources().getString(R.string.network_err_dialog_msg);
                dialogType = CommonDialogFragment.DIALOG_TYPE_NO_BUTTON;
                break;
            case BaseFragment.DIALOG_TYPE_DEBUG_MODE:
                String domain = DataManager.getInstance().getStringSharedOreferences("DOMAIN").trim().replaceAll("\"", "");
                title = getResources().getString(R.string.dialog_msg_debug_mode);
                message = domain;
                dialogType = CommonDialogFragment.DIALOG_TYPE_OK_BUTTON;
                break;
            default:
                return;
        }

        CommonDialogFragment fragment = CommonDialogFragment.newInstance(title, message, dialogType);
        fragment.setDialogListener(this);
        fragment.show(getSupportFragmentManager(), tag);
    }

    @Override
    public void setToolBarTitle(String title) {
    }

    @Override
    public void setVisibilityKeyWordView(boolean visible) {
    }

    @Override
    public void setKeyWord(String keyword) {
    }

    @Override
    public void doPositiveClick() {
    }

    @Override
    public void doNegativeClick() {
    }

    public boolean existForegroundFragment(@Nullable String tag) {
        FragmentManager manager = getSupportFragmentManager();
        int entryCount = manager.getBackStackEntryCount();
        if (entryCount > 0) {
            FragmentManager.BackStackEntry backEntry = manager.getBackStackEntryAt(entryCount - 1);
            String str = backEntry.getName();
            if (str != null && str.equals(tag)) {
                return true;
            }
        }

        return false;
    }

    private FragmentTransaction getCustomFragmentTransaction(int animation) {
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();

        switch (animation) {
            case ANIMATION_NONE:
                break;
            case ANIMATION_SLIDE_RIGHT_TO_LEFT:
                transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
                break;
            case ANIMATION_SLIDE_TOP_TO_BOTTOM:
                transaction.setCustomAnimations(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom, R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
                break;
            case ANIMATION_SLIDE_BOTTOM_TO_TOP:
                transaction.setCustomAnimations(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top, R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
                break;
            case ANIMATION_SLIDE_LEFT_TO_RIGHT:
                transaction.setCustomAnimations(R.anim.slide_in_from_left, R.anim.slide_out_to_right, R.anim.slide_in_from_right, R.anim.slide_out_to_left);
                break;
            default:
                break;
        }

        return transaction;
    }

    @Override
    public void showDialogFragment(String title, String message, DialogListener listener, int dialogType, String tag) {
        // ダイアログの表示
        CommonDialogFragment fragment = CommonDialogFragment.newInstance(title, message, dialogType);
        fragment.setDialogListener(listener == null ? this : listener);
        fragment.show(getSupportFragmentManager(), tag);
    }

    @Override
    public void showDialogFragment(String value, int dialogType, String tag) {
        // ダイアログの表示
        CommonDialogFragment fragment = CommonDialogFragment.newInstance(value, dialogType);
        fragment.setDialogListener(this);
        fragment.show(getSupportFragmentManager(), tag);
    }

    @Override
    public boolean setDialogListener(DialogListener listener) {
        FragmentManager fm = getSupportFragmentManager();
        List<Fragment> fragments = fm.getFragments();
        for (Fragment f : fragments) {
            if (f != null && f instanceof CommonDialogFragment) {
                ((CommonDialogFragment) f).setDialogListener(listener);
                return true;
            }
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    public Drawable getDrawableResource(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return getDrawable(id);
        } else {
            return getResources().getDrawable(id);
        }
    }
}
