package qalatt.hoctuvung.mimosa.views.customviews;

import android.app.Dialog;
import android.content.Context;

/**
 * Created by manh on 6/15/18.
 */

public class CustomProgressDialog extends Dialog {
    private CallbackListener mCallbackListener;

    public CustomProgressDialog(Context context) {
        super(context);
        Create();
    }

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
        Create();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mCallbackListener != null) {
            mCallbackListener.onBackPressed();
        }
    }

    public void setCallbackListener(CallbackListener listener) {
        mCallbackListener = listener;
    }

    private void Create() {
//		LayoutInflater i = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       // setContentView(R.layout.common_loading);
    }

    public interface CallbackListener {
        void onBackPressed();
    }
}

