package qalatt.hoctuvung.mimosa.views.activites;


import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


import butterknife.ButterKnife;
import qalatt.hoctuvung.mimosa.R;
import qalatt.hoctuvung.mimosa.views.fragments.HomeFragment;

public class MainActivity extends BaseActivity {
    private AdView mAdView;
    ViewGroup.LayoutParams mAdViewParams;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mAdView = findViewById(R.id.adView);
//        showAdsAdmob(true);
        addFragment(HomeFragment.newInstance(), BaseActivity.ANIMATION_SLIDE_RIGHT_TO_LEFT);

//        if (!DataManager.getInstance().existFirstStart() || !DataManager.getInstance().loadFirstStart()) {
//            fragment = HomeFragment.newInstance();
//            DataManager.getInstance().saveFirstStart(true);
//            addFragment(fragment, ANIMATION_NONE);
//        } else {
//
//        }

    }
    protected void showAdsAdmob(boolean isBanner) {
        if (isBanner) {
            AdRequest adRequest = new AdRequest.Builder().addTestDevice("7903FA508994B334D9C25E3401C09EF5").build();
            mAdViewParams = mAdView.getLayoutParams();
            mAdViewParams.height = 0;
            mAdView.loadAd(adRequest);
            mAdView.setLayoutParams(mAdViewParams);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mAdViewParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    mAdView.setLayoutParams(mAdViewParams);
                }
            });
        }
    }



    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("MIMOSA");
        builder.setMessage("Bạn có muốn thoát ứng dụng không?");
        builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });
        builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
