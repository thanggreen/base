package qalatt.hoctuvung.mimosa.views.customviews;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import qalatt.hoctuvung.mimosa.R;
import qalatt.hoctuvung.mimosa.commons.AppConstants;
import qalatt.hoctuvung.mimosa.interfaces.DialogListener;

/**
 * Created by manh on 6/15/18.
 */

public class CommonDialogFragment extends DialogFragment {
    public static final int DIALOG_TYPE_NO_BUTTON = 0;
    public static final int DIALOG_TYPE_OK_BUTTON = 1;
    public static final int DIALOG_TYPE_OK_CANCEL_BUTTON = 2;
    public static final int DIALOG_TYPE_YES_OR_NO = 3;
    public static final int DIALOG_TYPE_NO_OR_YES = 4;

    public static final int DIALOG_TYPE_TELEPHONE = 98;
    protected static final String PARAM_TITLE = "title";
    protected static final String PARAM_MESSAGE = "massage";
    protected static final String PARAM_TYPE = "type";
    protected static final String PARAM_STRING_VALUE = "string_value";
    public static final String DIALOG_TAG = "dialog_%s";
    private static final String SAVE_TYPE_DIALOG = "saveType.dialog";
    private static final int mShowTime = AppConstants.DIALOG_DISPLAY_TIME;
    public final String FRAGMENT_TAG = getClass().getSimpleName();
    protected DialogListener mListener;
    private boolean mCancel = false;
    private static int mType;
    private static boolean shown = false;
    private static String dialogTag;

    public static CommonDialogFragment newInstance(String title, String message, int type) {
        CommonDialogFragment fragment = new CommonDialogFragment();
        Bundle arguments = new Bundle();
        arguments.putString(PARAM_TITLE, title);
        arguments.putString(PARAM_MESSAGE, message);
        arguments.putInt(PARAM_TYPE, type);
        fragment.setArguments(arguments);

        return fragment;
    }

    public static CommonDialogFragment newInstance(String value, int type) {
        CommonDialogFragment fragment = new CommonDialogFragment();
        Bundle arguments = new Bundle();
        arguments.putString(PARAM_STRING_VALUE, value);
        arguments.putInt(PARAM_TYPE, type);
        fragment.setArguments(arguments);

        return fragment;
    }

    public void setDialogListener(DialogListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            // 復帰処理
            mType = savedInstanceState.getInt(SAVE_TYPE_DIALOG);
        }
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString(PARAM_TITLE);
        String message = getArguments().getString(PARAM_MESSAGE);
        final String value = getArguments().getString(PARAM_STRING_VALUE);
        mType = getArguments().getInt(PARAM_TYPE);
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message);

        switch (mType) {
            case DIALOG_TYPE_OK_BUTTON:

                alert.setPositiveButton(getResources().getString(R.string.common_dialog_button_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mListener != null) mListener.doPositiveClick();
                        dismiss();
                    }
                });
                break;
            case DIALOG_TYPE_OK_CANCEL_BUTTON:

                alert.setPositiveButton(getResources().getString(R.string.common_dialog_button_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (mListener != null) mListener.doPositiveClick();
                        dismiss();
                    }
                });

                alert.setNegativeButton(getResources().getString(R.string.common_dialog_button_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (mListener != null) mListener.doNegativeClick();
                        dismiss();
                    }
                });
                break;
            case DIALOG_TYPE_NO_BUTTON:

                this.setCancelable(false);

                Handler hdl = new Handler();
                hdl.postDelayed(new dialogHandler(), mShowTime);

                break;
            case DIALOG_TYPE_YES_OR_NO:
                alert.setPositiveButton(getResources().getString(R.string.common_dialog_button_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mListener != null) mListener.doPositiveClick();
                        dismiss();
                    }
                });
                alert.setNegativeButton(getResources().getString(R.string.common_dialog_button_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (mListener != null) mListener.doNegativeClick();
                        dismiss();
                    }
                });
                break;
            case DIALOG_TYPE_NO_OR_YES:
                alert.setPositiveButton(getResources().getString(R.string.common_dialog_button_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mListener != null) mListener.doPositiveClick();
                        dismiss();
                    }
                });
                alert.setNegativeButton(getResources().getString(R.string.common_dialog_button_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mListener != null) mListener.doNegativeClick();
                        dismiss();
                    }
                });
                break;
            default:
                break;
        }

        return alert.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //ダイアログ表示タイプ保存
        outState.putInt(SAVE_TYPE_DIALOG, mType);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        tag = String.format(DIALOG_TAG, tag);
        if (shown && this.dialogTag.equals(tag)) return;

        // 同一ダイアログが既に表示している時は表示を行わない
        if (manager.findFragmentByTag(tag) != null) {
            return;
        }
        mCancel = false;
        shown = true;
        dialogTag = tag;
        super.show(manager, tag);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        shown = false;
        dialogTag = "";
        super.onDismiss(dialog);
    }

    @Override
    public void onPause() {
        dismiss();
        if (mType == DIALOG_TYPE_NO_BUTTON) {
            mCancel = true;
        }
        super.onPause();
    }

    /**
     * ダイアログを閉じる
     */
    class dialogHandler implements Runnable {
        public void run() {
            if (mCancel) return;
            dismiss();
        }
    }
}
