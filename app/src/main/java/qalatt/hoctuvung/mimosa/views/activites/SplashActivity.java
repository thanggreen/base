package qalatt.hoctuvung.mimosa.views.activites;


import android.content.BroadcastReceiver;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.daimajia.numberprogressbar.OnProgressBarListener;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.google.android.gms.ads.MobileAds;

import java.util.Locale;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import qalatt.hoctuvung.mimosa.R;
import qalatt.hoctuvung.mimosa.adapters.adNative.InterstitialUtils;

public class SplashActivity extends BaseActivity implements OnProgressBarListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

//        MobileAds.initialize(this, getResources().getString(R.string.adm_app_id));
        InterstitialUtils.getSharedInstance().init(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                InterstitialUtils.getSharedInstance().showInterstitialAd(new InterstitialUtils.AdCloseListener() {
//                    @Override
//                    public void onAdClosed() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
//                    }
//                });
            }
        }, 3000);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onProgressChange(int current, int max) {

    }




}
