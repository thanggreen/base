package qalatt.hoctuvung.mimosa.views.fragments;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.lang.reflect.Field;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import butterknife.ButterKnife;
import me.yokeyword.swipebackfragment.SwipeBackFragment;
import qalatt.hoctuvung.mimosa.R;
import qalatt.hoctuvung.mimosa.commons.CommonUtils;
import qalatt.hoctuvung.mimosa.commons.LogUtils;
import qalatt.hoctuvung.mimosa.interfaces.DialogListener;
import qalatt.hoctuvung.mimosa.interfaces.InternetStateListener;
import qalatt.hoctuvung.mimosa.managers.broadcasts.NetworkChangeReceiver;
import qalatt.hoctuvung.mimosa.networkings.RetrofitError;
import qalatt.hoctuvung.mimosa.presenters.BasePresenter;
import qalatt.hoctuvung.mimosa.views.activites.BaseActivity;
import qalatt.hoctuvung.mimosa.views.customviews.CommonDialogFragment;
import qalatt.hoctuvung.mimosa.views.customviews.CustomProgressDialog;

public abstract class BaseFragment extends SwipeBackFragment implements InternetStateListener, CustomProgressDialog.CallbackListener {
    public static final int DIALOG_TYPE_NETWORK_ERROR = 0;
    public static final int DIALOG_TYPE_API_ERROR = 1;
    public static final int DIALOG_TYPE_DEBUG_MODE = 2;

    protected static final boolean VISIBLE = true;
    protected static final boolean INVISIBLE = false;

    protected static final int PERMISSION_STORAGE_WRITE_REQUEST_CODE = 2;
    private static final int PERMISSION_CALL_PHONE_REQUEST_CODE = 1;
    public final String FRAGMENT_TAG = getClass().getSimpleName();

    protected ChangeFragmentListener mChangeFragmentListener;
    protected ChangeToolBarListener mChangeToolBarListener;

    private String mCallNo;
    private CustomProgressDialog mCustomProgressDialog;
    private View mView;

    private boolean requestCall = false;

    protected BasePresenter mPresenter = null;
    public abstract int getLayoutId();
    protected View overlayView;
    private BaseActivity mBaseActivity;
    // Internet state
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isNetworkAvailable = intent.getBooleanExtra(NetworkChangeReceiver.IS_NETWORK_AVAILABLE, false);
            isNetworkAvailable(isNetworkAvailable);
        }
    };

    private void registerNetwork(){
        IntentFilter intentFilter = new IntentFilter(NetworkChangeReceiver.NETWORK_AVAILABLE_ACTION);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(networkChangeReceiver ,intentFilter );
    }

    protected void unregisterNetwork() {
        try {
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(networkChangeReceiver);
        } catch (IllegalArgumentException e) {
            LogUtils.e(e.getMessage());
            e.printStackTrace();
        }
    }
    public abstract void initView();

    public abstract void initData();



    @Override
    public void isNetworkAvailable(boolean isNetworkAvailable) {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(mBaseActivity).inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, view);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        initView();
        initData();
        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        registerNetwork();
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterNetwork();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        if (activity instanceof BaseActivity) {
            mBaseActivity = (BaseActivity) activity;
        }

        if (!(activity instanceof ChangeFragmentListener)) {
            throw new ClassCastException("activity ChangeFragmentListener");
        }
        mChangeFragmentListener = ((ChangeFragmentListener) activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mChangeFragmentListener = null;
        mChangeToolBarListener = null;
    }

    @Override
    public void onDestroy() {

        try {
            Field mSwipeBackLayout = SwipeBackFragment.class.getDeclaredField("mSwipeBackLayout");
            mSwipeBackLayout.setAccessible(true);
            mSwipeBackLayout.set(this, null);
        } catch (NoSuchFieldException e) {
            LogUtils.e(e.getMessage());
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            LogUtils.e(e.getMessage());
            e.printStackTrace();
        }

        try {
            Field mNoAnim = SwipeBackFragment.class.getDeclaredField("mNoAnim");
            mNoAnim.setAccessible(true);
            mNoAnim.set(this, null);
        } catch (NoSuchFieldException e) {
            LogUtils.e(e.getMessage());
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            LogUtils.e(e.getMessage());
            e.printStackTrace();
        }

        super.onDestroy();
    }

    protected void showActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.show();
        }
    }

    protected void hideActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    protected void setVisibilityNavigationIcon(boolean visible) {
        ActionBar actionBar = getActionBar();
        if (actionBar == null) {
            return;
        }

        if (visible) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        } else {
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    protected void setVisibilityHomeLogo(boolean visible) {
        ActionBar actionBar = getActionBar();
        if (actionBar == null) {
            return;
        }
        actionBar.setDisplayUseLogoEnabled(visible);
    }

    protected void setToolBarTitle(String title) {
        mChangeToolBarListener.setToolBarTitle(title);
    }

    protected void setVisibilityKeyWordView(boolean visible) {
        mChangeToolBarListener.setVisibilityKeyWordView(visible);
    }

    public void showConnectingDialog() {
        dismissConnectingDialog();

        mCustomProgressDialog = new CustomProgressDialog(getActivity(), R.style.Theme_Holo_Light_Dialog);
        mCustomProgressDialog.show();
        mCustomProgressDialog.setCallbackListener(this);
    }

    public void dismissConnectingDialog() {
        if (mCustomProgressDialog != null && mCustomProgressDialog.isShowing()) {
            mCustomProgressDialog.dismiss();
            mCustomProgressDialog = null;
        }
    }

    public void showDialog(int type) {
        mChangeFragmentListener.showDialogFragment(type, FRAGMENT_TAG);
    }

    public void showDialog(RetrofitError error, boolean isPopBackStack){
        if(error == null) return;
        Throwable throwable = error.getCause();
        if(throwable instanceof UnknownHostException || throwable instanceof ConnectException
                || throwable instanceof TimeoutException || throwable instanceof SocketTimeoutException){
            showOfflineDialog(isPopBackStack);
        } else {
            showApiErrorDialog(isPopBackStack);
        }
    }

    public void showOfflineDialog(final boolean isPopBackStack){
        String title = getResources().getString(R.string.offline_err_dialog_title);
        String message = getResources().getString(R.string.offline_err_dialog_msg);
        int dialogType = CommonDialogFragment.DIALOG_TYPE_OK_BUTTON;
        String tag = "RetrofitError";
        showDialog(title, message,
                new DialogListener() {
                    @Override
                    public void doPositiveClick() {
                        if(isPopBackStack){
                            getFragmentManager().popBackStack();
                        }
                    }

                    @Override
                    public void doNegativeClick() {
                    }
                },
                dialogType, tag);
    }

    public void showApiErrorDialog(final boolean isPopBackStack){
        String title = getResources().getString(R.string.api_err_dialog_title);
        String message = getResources().getString(R.string.api_err_dialog_msg);
        int dialogType = CommonDialogFragment.DIALOG_TYPE_OK_BUTTON;
        String tag = "RetrofitError";
        showDialog(title, message, new DialogListener() {
                    @Override
                    public void doPositiveClick() {
                        if(isPopBackStack){
                            getFragmentManager().popBackStack();
                        }
                    }

                    @Override
                    public void doNegativeClick() {
                    }
                },
                dialogType, tag);
    }

    private ActionBar getActionBar() {
        Activity activity = getActivity();
        if (activity instanceof AppCompatActivity) {
            return ((AppCompatActivity) activity).getSupportActionBar();
        }

        return null;
    }

    public void showDialog(String title, String message, DialogListener listener, int dialogType) {
        mChangeFragmentListener.showDialogFragment(title, message, listener, dialogType, FRAGMENT_TAG);
    }

    public void showDialog(String title, String message, DialogListener listener, int dialogType, String tag) {
        mChangeFragmentListener.showDialogFragment(title, message, listener, dialogType, tag);
    }

    public boolean setDialogListener(DialogListener listener) {
        return mChangeFragmentListener.setDialogListener(listener);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CALL_PHONE_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestCall = true;
                }
                break;
            default:
                break;
        }
    }

    public BaseFragment replaceFragment(Class classObject) {
        return replaceFragment(classObject, false);
    }

    public BaseFragment replaceFragment(Class classObject, boolean isBackKey) {
        if (mChangeFragmentListener == null || !CommonUtils.singleClickEvent() || getFragmentManager() == null) {
            if (getFragmentManager() == null) {

                LogUtils.d("error. getFragmentManager is null");
            } else if (!CommonUtils.singleClickEvent()) {

                LogUtils.d("error. doubleClick.");
            } else {

                LogUtils.d("error. mChangeFragmentListener is null.");
            }
            return null;
        }

        BaseFragment fragment = (BaseFragment) getFragmentManager().findFragmentByTag(classObject.getSimpleName());
        if (fragment == null) {
            //check exist fragment
//            if (classObject.equals(BikeListBaseFragment.class)) {
//                fragment = BikeListBaseFragment.newInstance();
//            } else if (classObject.equals(ClientTabFragment.class)) {
//                fragment = ClientTabFragment.newInstance();
//            } else if (classObject.equals(ConditionsFragment.class)) {
//                fragment = ConditionsFragment.newInstance();
//            } else if (classObject.equals(InformationFragment.class)) {
//                fragment = InformationFragment.newInstance();
//            } else
            {
                return null;
            }

          //  mChangeFragmentListener.addFragment(fragment, BaseActivity.ANIMATION_SLIDE_RIGHT_TO_LEFT);
        } else {
            // 作成済
            mChangeFragmentListener.popBackStackFragment(fragment.getClass().getSimpleName());
        }

        return fragment;
    }

    @Override
    public void onBackPressed() {

    }

    public interface ChangeFragmentListener {
        void addFragment(BaseFragment fragment, int animation);

        void addFragmentWithTarget(BaseFragment parent, BaseFragment child, int animation, int fragmentCode);

        void addFragment(BaseFragment fragment, int animation, int resId);

        void changeFragment(BaseFragment fragment, int animation);

        void changeFragment(BaseFragment fragment, int animation, int resId);

        void replaceChildFragment(BaseFragment fragment, int animation, BaseFragment parentFragment);

        void replaceChildFragment(BaseFragment fragment, int animation, BaseFragment parentFragment, int resId);

        void popBackStackInclusiveFragment(String stackName);

        void popBackStackFragment(String stackName);

        void popToRootFragments();

        void popBackStackFragments();

        boolean existForegroundFragment(String tag);

        void showDialogFragment(int type, String tag);

        void showDialogFragment(String value, int type, String tag);

        void showDialogFragment(String title, String message, DialogListener listener, int dialogType, String tag);

        boolean setDialogListener(DialogListener listener);
    }


    public interface ChangeToolBarListener {
        void setToolBarTitle(String title);

        void setKeyWord(String keyword);

        void setVisibilityKeyWordView(boolean visible);
    }
}
