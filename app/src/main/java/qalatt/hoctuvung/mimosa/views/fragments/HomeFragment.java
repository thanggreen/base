package qalatt.hoctuvung.mimosa.views.fragments;


import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import devlight.io.library.ntb.NavigationTabBar;
import qalatt.hoctuvung.mimosa.R;
import qalatt.hoctuvung.mimosa.adapters.CustomFragmentPagerAdapter;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.tbTabBar)
    NavigationTabBar tbTabBar;

    @BindView(R.id.contentPanel)
    ViewPager contentPage;
    private CustomFragmentPagerAdapter customFragmentPagerAdapter;
    private List<BaseFragment> baseFragments = new ArrayList<>();
    public static HomeFragment newInstance() {
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseFragments.add(new LearnFragment());
        baseFragments.add(new LearnFragment());
        baseFragments.add(new LearnFragment());
    }

    @Override
    public int getLayoutId() {
        return R.layout.home_fragment;
    }

    @Override
    public void initView() {
        unregisterNetwork();
    }

    @Override
    public void initData() {
        initTabBar();

    }

    private void initTabBar() {

        contentPage.setOffscreenPageLimit(3);
        customFragmentPagerAdapter = new CustomFragmentPagerAdapter(baseFragments, null, getChildFragmentManager());
        contentPage.setOffscreenPageLimit(baseFragments.size());
        contentPage.setAdapter(customFragmentPagerAdapter);
        final String[] colors = getResources().getStringArray(R.array.default_preview);


        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();

        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_launcher_background),
                        Color.TRANSPARENT)
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_launcher_background))
                        .title("Từ vựng")
//                        .badgeTitle("with")
                        .build()
        );

        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_launcher_background),
                        Color.TRANSPARENT)
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_launcher_background))
                        .title("Luyện tập")
                        .badgeTitle("NTB")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_launcher_background),
                        Color.TRANSPARENT)
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_launcher_background))
                        .title("Cài đặt")
                        .build()
        );


        tbTabBar.setModels(models);
        tbTabBar.setViewPager(contentPage, 1);
        tbTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                tbTabBar.getModels().get(position).hideBadge();
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

        tbTabBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < tbTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = tbTabBar.getModels().get(i);
                    tbTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            model.showBadge();
                        }
                    }, i * 100);
                }
            }
        }, 500);
    }

}
