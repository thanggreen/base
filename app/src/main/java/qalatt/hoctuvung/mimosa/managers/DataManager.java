package qalatt.hoctuvung.mimosa.managers;


import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import qalatt.hoctuvung.mimosa.BuildConfig;



public class DataManager {
    private static final String PREFERENCE_NAME = "pettvs.com.android.DataManager";
    private static final String EXIST_KEY = "false";
    private static DataManager instance;
    private static final String PREFERENCE_KEY_FIRST_START = "FirstStart";
    private static final String PREFERENCE_KEY_START_UP = "StartUp";
    private static final String PREFERENCE_KEY_HOME_DATA = "HomeData";
    private static final String PREFERENCE_KEY_CATE_LIST = "CateList";
    private static final String PREFERENCE_KEY_LIKE_LIST = "LikedList";
    private static final String PREFERENCE_KEY_DOWNLOAD_LIST = "DownloadList";
    private static final String WIDTH_SCREEN_KEY = "widthScreen";
    private static final String HEIGHT_SCREEN_KEY = "heightScreen";


    public synchronized static DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();

        }

        return instance;
    }

    private SharedPreferences getPreferences() {
        return EConvarSApplication.getInstance().getPreferences(PREFERENCE_NAME);
    }

    public boolean exist(String key) {
        SharedPreferences preferences = getPreferences();
        key = preferences.getString(key, EXIST_KEY);
        if (key == null) {
            return false;
        } else if (key.equals(EXIST_KEY)) {
            return false;
        }

        return true;
    }

    public void save(String key, Object data) {
        SharedPreferences preferences = getPreferences();
        Gson gson = new Gson();
        preferences.edit().putString(key, gson.toJson(data)).apply();
    }

    public void ClearKey(String key) {
        SharedPreferences preferences = getPreferences();
    }

    private <T> T load(String key, Type type) {
        SharedPreferences preferences = getPreferences();
        Gson gson = new Gson();
        return gson.fromJson(preferences.getString(key, null), type);
    }

    public String getStringSharedOreferences(String key) {
        SharedPreferences preferences = getPreferences();
        return preferences.getString(key, "");
    }


    private int addSharedPreferencesIntValue(String key, int value) {
        int count = 0;
        if (exist(key)) {
            count = load(key, new TypeToken<Integer>() {
            }.getType());
            count += value;
            save(key, count);
        } else {
            save(key, value);
            count = value;
        }

        return count;
    }

    public Boolean existPreference() {
        return getPreferences().getAll().size() > 0;
    }

    public void allClear() {
        if (BuildConfig.DEBUG) {
            SharedPreferences preferences = getPreferences();
            preferences.edit().clear().apply();
        }
    }

    public boolean existFirstStartState() {
        return exist(PREFERENCE_KEY_START_UP);
    }

    public void saveFirstStartState(boolean initialized) {
        save(PREFERENCE_KEY_START_UP, initialized);
    }


    public boolean loadFirstStartState() {
        return load(PREFERENCE_KEY_START_UP, new TypeToken<Boolean>() {
        }.getType());
    }

    public boolean existFirstStart() {
        return exist(PREFERENCE_KEY_FIRST_START);
    }


    public boolean loadFirstStart() {
        return load(PREFERENCE_KEY_FIRST_START, new TypeToken<Boolean>() {
        }.getType());
    }


    public void saveFirstStart(boolean isInitialized) {
        save(PREFERENCE_KEY_FIRST_START, isInitialized);
    }

    public void addWidthScreen(int width) {
        save(WIDTH_SCREEN_KEY, width);
    }

    public void addHeightScreen(int height) {
        save(HEIGHT_SCREEN_KEY, height);
    }

    public int getWidthScreen() {
        return load(WIDTH_SCREEN_KEY, Integer.class);
    }

    public int getHeightScreen() {
        return load(HEIGHT_SCREEN_KEY, Integer.class);
    }

}
