package qalatt.hoctuvung.mimosa.managers.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import qalatt.hoctuvung.mimosa.commons.ConnectivityUtil;


public class NetworkChangeReceiver extends BroadcastReceiver {

    public static final String NETWORK_AVAILABLE_ACTION = "qalatt.hoctuvung.mimosa";
    public static final String IS_NETWORK_AVAILABLE = "isNetworkAvailable";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent networkStateIntent = new Intent(NETWORK_AVAILABLE_ACTION);
        networkStateIntent.putExtra(IS_NETWORK_AVAILABLE,  ConnectivityUtil.isNetworkConnected());
        LocalBroadcastManager.getInstance(context).sendBroadcast(networkStateIntent);
    }
}
