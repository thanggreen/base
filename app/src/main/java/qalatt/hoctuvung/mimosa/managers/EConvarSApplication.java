package qalatt.hoctuvung.mimosa.managers;


import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.multidex.MultiDex;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import qalatt.hoctuvung.mimosa.BuildConfig;


public class EConvarSApplication extends Application {
    private static EConvarSApplication sInstance;
    private static final long MAX_ACTIVITY_TRANSITION_TIME_MS = 2000;
    public boolean wasInBackground;
    private Timer mActivityTransitionTimer;
    private TimerTask mActivityTransitionTimerTask;

    public EConvarSApplication() {
        super();
    }

    public static EConvarSApplication getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        sInstance = this;
        ButterKnife.setDebug(BuildConfig.DEBUG);

        DataManager manager = DataManager.getInstance();

        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .setReadTimeout(30_000)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);

        if(BuildConfig.DEBUG) {
            manager.saveFirstStartState(false);
        }

    }

    public SharedPreferences getPreferences(String name) {
        return getApplicationContext().getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public void startActivityTransitionTimer() {
        this.mActivityTransitionTimer = new Timer();
        this.mActivityTransitionTimerTask = new TimerTask() {
            public void run() {

                ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
                List<ActivityManager.RunningAppProcessInfo> processInfoList = am.getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo info : processInfoList) {
                    if (info.processName.equals(getPackageName()) && info.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        // ここにフォアグラウンドの場合の処理を実装
                        EConvarSApplication.this.wasInBackground = true;
                        if (mActivityTransitionTimerTask != null) {
                            mActivityTransitionTimerTask.cancel();
                        }
                    }
                }
            }
        };

        this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask,
                MAX_ACTIVITY_TRANSITION_TIME_MS);
    }

    public void stopActivityTransitionTimer() {
        if (this.mActivityTransitionTimerTask != null) {
            this.mActivityTransitionTimerTask.cancel();
        }

        if (this.mActivityTransitionTimer != null) {
            this.mActivityTransitionTimer.cancel();
        }

        this.wasInBackground = false;
    }

}
