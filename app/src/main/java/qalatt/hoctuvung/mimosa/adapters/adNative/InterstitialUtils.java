package qalatt.hoctuvung.mimosa.adapters.adNative;


import android.content.Context;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class InterstitialUtils {


    private static InterstitialUtils sharedInstance;
    private InterstitialAd interstitialAd;
//        private final String APP_ID = "ca-app-pub-4930631223596955~3277781754";
//    private final String INTERSTITIAL_AD_UNTI = "ca-app-pub-4930631223596955/1716647444";
    private final String INTERSTITIAL_AD_UNTI = "ca-app-pub-3940256099942544/1033173712";
    private final String APP_ID = "ca-app-pub-3940256099942544~3347511713";


    private AdCloseListener adCloseListener;
    private boolean isReloaded = false;

    public static InterstitialUtils getSharedInstance() {
        if (sharedInstance == null) {
            sharedInstance = new InterstitialUtils();
        }

        return sharedInstance;
    }


    public void init(Context context) {
        MobileAds.initialize(context, APP_ID);
        interstitialAd = new InterstitialAd(context);
        interstitialAd.setAdUnitId(INTERSTITIAL_AD_UNTI);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                if (adCloseListener != null) {
                    adCloseListener.onAdClosed();
                } else {
                    loadInterstitialAd();
                }
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                if (isReloaded == false) {
                    isReloaded = true;
                    loadInterstitialAd();
                }
            }
        });
        loadInterstitialAd();
    }


    public void showInterstitialAd(AdCloseListener adCloseListener) {
        if (canShowInterstitialAd()) {
            isReloaded = false;
            this.adCloseListener = adCloseListener;
            interstitialAd.show();
        } else {
            loadInterstitialAd();
            adCloseListener.onAdClosed();
        }
    }


    private boolean canShowInterstitialAd() {
        return interstitialAd != null && interstitialAd.isLoaded();
    }

    private void loadInterstitialAd() {
        if (interstitialAd != null && !interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder().addTestDevice("7903FA508994B334D9C25E3401C09EF5").build();
            interstitialAd.loadAd(adRequest);
        }
    }


    public interface AdCloseListener {
        void onAdClosed();
    }
}
