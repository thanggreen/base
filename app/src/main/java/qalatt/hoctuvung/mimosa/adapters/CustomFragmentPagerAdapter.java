package qalatt.hoctuvung.mimosa.adapters;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

import qalatt.hoctuvung.mimosa.views.fragments.BaseFragment;

/**
 * Created by thanh on 10/4/2018.
 * Des:
 */

public class CustomFragmentPagerAdapter extends FragmentStatePagerAdapter {
    private List<BaseFragment> mListFragment;
    private List<String> mListTitleFragment;

    public CustomFragmentPagerAdapter(List<BaseFragment> listFragment, List<String> listTitleFragment, FragmentManager fm) {
        super(fm);
        mListFragment = listFragment;
        mListTitleFragment = listTitleFragment;
    }

    @Override
    public Fragment getItem(int position) {
        if (mListFragment != null && position < mListFragment.size()) {
            return mListFragment.get(position);
        }
        return null;
    }

    @Override
    public int getCount() {
        if (mListFragment != null) {
            return mListFragment.size();
        }
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mListTitleFragment != null && position < mListTitleFragment.size()) {
            return mListTitleFragment.get(position);
        }
        return null;
    }


}
