package qalatt.hoctuvung.mimosa.presenters.eventbus;

import retrofit2.Response;

public class BaseEventBus {
    protected Response mResponse;

    public Response getResponse() {
        return mResponse;
    }
}
