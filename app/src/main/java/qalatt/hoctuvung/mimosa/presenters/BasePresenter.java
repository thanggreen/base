package qalatt.hoctuvung.mimosa.presenters;

import android.content.Context;

import qalatt.hoctuvung.mimosa.networkings.ApiClient;
import qalatt.hoctuvung.mimosa.views.fragments.BaseFragment;



public class BasePresenter {
    protected BaseFragment mFragment;
    private Context mContext;
    protected ApiClient mApiClient = new ApiClient();
    public BasePresenter(BaseFragment fragment) {
        this.mFragment = fragment;
        this.mContext = fragment.getActivity();
    }

    public BasePresenter(Context context) {
        this.mFragment = null;
        this.mContext = context;
    }

    public void onDestroy() {
        mFragment = null;
        mContext = null;
    }
}
